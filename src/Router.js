import { createStackNavigator } from 'react-navigation';
import Main from './components/Main';
import Calculator from './components/Calculator';

export default  RootStack = createStackNavigator({
    Main: { screen: Main },
    Calculator: { screen: Calculator }
})