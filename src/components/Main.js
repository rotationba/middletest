import React, { Component } from "react";
import _ from 'lodash';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  FlatList
} from "react-native";
import Title from "./Title";
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number1: "",
      number2: "",
      histories: []
    };
  }
  static navigationOptions = {
    headerTitle: "MiddleTest",
    headerRight: null
  };

  componentWillReceiveProps(nextProps) {
    operator = nextProps.navigation.getParam("operator");
    console.log(operator);
    const { number1, number2, histories } = this.state;
    const history = {
      number1,
      number2,
      operator: operator === "div" ? "/" : "-",
      result:
        operator === "div"
          ? parseFloat(number1) / parseFloat(number2)
          : parseFloat(number1) - parseFloat(number2)
    };
    this.setState({ histories: [...histories, history] });
  }

  handleSend = () => {
    const { number1, number2 } = this.state;
    console.log(_.isNumber(8.9));

    if (!number1 || !number2 || !_.isNumber(parseFloat(number1)) || !_.isNumber(parseFloat(number2))) alert("Bạn cần nhập đủ thông tin hoặc sai định dạng");
    else {
      this.props.navigation.navigate("Calculator", { number1, number2 });
    }
  };

  render() {
    console.log(this.state);
    return (
      <View style={{ padding: 15 }}>
        <Title />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 15
          }}
        >
          <Text style={{ fontSize: 20, marginRight: 15 }}>Number 1:</Text>
          <TextInput
            style={{
              borderWidth: 1,
              borderColor: "#000",
              width: 200,
              padding: 5
            }}
            keyboardType={"numeric"}
            value={this.state.number1}
            onChangeText={number1 => this.setState({ number1 })}
          />
        </View>

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 15
          }}
        >
          <Text style={{ fontSize: 20, marginRight: 15 }}>Number 2:</Text>
          <TextInput
            style={{
              borderWidth: 1,
              borderColor: "#000",
              width: 200,
              padding: 5
            }}
            keyboardType={"numeric"}
            value={this.state.number2}
            onChangeText={number2 => this.setState({ number2 })}
          />
        </View>

        <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
          <TouchableOpacity
            style={{ padding: 15, backgroundColor: "#fff", borderRadius: 5 }}
            onPress={this.handleSend}
          >
            <Text>SEND</Text>
          </TouchableOpacity>
        </View>
        <FlatList
        style={{ height: 300 }}
          data={this.state.histories}
          renderItem={item => {
              console.log(item);
              return <View style={{ borderBottomColor: '#000', borderBottomWidth: 1, justifyContent: 'center', paddingVertical: 10 }} >
                  <Text>{`${item.item.number1} ${item.item.operator} ${item.item.number2} = ${item.item.result}`}</Text>
                </View>;
          }}
        />
      </View>
    );
  }
}

export default Main;
