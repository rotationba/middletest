import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Title extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ alignItems: 'center', paddingVertical: 5, marginVertical: 15, backgroundColor: '#fff' }} >
        <Text style={{ fontSize: 18, fontWeight: 'bold' }} >CALCULATOR</Text>
      </View>
    );
  }
}

export default Title;
