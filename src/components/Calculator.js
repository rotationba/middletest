import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Title from "./Title";
class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number1: "",
      number2: ""
    };
  }
  static navigationOptions = {
    headerTitle: "MiddleTest",
    headerLeft: null
  };
  componentWillMount() {
    const number1 = this.props.navigation.getParam("number1");
    const number2 = this.props.navigation.getParam("number2");
    this.setState({ number1, number2 });
  }
  handleSub = () => {
    const { navigate } = this.props.navigation;
    navigate('Main', {operator: 'sub'})
  }
  handleDiv = () => {
    const { navigate } = this.props.navigation;
    navigate('Main', { operator: 'div' })
  }

  render() {
    return (
      <View style={{ padding: 15 }}>
        <Title />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 15
          }}
        >
          <Text style={{ fontSize: 20, marginRight: 15 }}>Number 1:</Text>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            {this.state.number1}
          </Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 15
          }}
        >
          <Text style={{ fontSize: 20, marginRight: 15 }}>Number 2:</Text>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            {this.state.number2}
          </Text>
        </View>

        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
          <TouchableOpacity
            style={{
              padding: 15,
              backgroundColor: "#fff",
              borderRadius: 5,
              width: 100,
              alignItems: "center"
            }}
            onPress={this.handleSub}
          >
            <Text>-</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              padding: 15,
              backgroundColor: "#fff",
              borderRadius: 5,
              width: 100,
              alignItems: "center"
            }}
            onPress={this.handleDiv}
          >
            <Text>/</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Calculator;
